# **Introduction**
------
Hardware and system time are very important for some services provided by
server. This guide is designed to configure hardware time and system time.

# **Usage**
------
Please use hwclock and date command to configure the hardware time and
system time, respectively.

Firstly, see the hardware time and system time.
```
$ hwclock -r
$ date
```
If the timing are not accurate, using the following command to set the
accurate time.

Set the hardware time.
```
$ hwclock --set --date "9/19/2016 23:10:45"
```
Synchronize the hardware time to system time.
```
$ hwclock -s
```
Synchronize the system time to hardware time.
```
$ hwclock -w
```
Observe the time by hardware and system.
```
$ hwclock -r
$ date
```

# **Author**

Jason Chen
